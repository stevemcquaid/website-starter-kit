package handlers

import (
	"context"
	"fmt"
	"net/http"

	"geeks-accelerator/oss/website-starter-kit/internal/platform/web"
	"geeks-accelerator/oss/website-starter-kit/internal/platform/web/webcontext"
	"geeks-accelerator/oss/website-starter-kit/internal/webroute"

	"github.com/ikeikeikeike/go-sitemap-generator/v2/stm"
)

// Root represents the Root API method handler set.
type Root struct {
	StaticDir string
	Renderer  web.Renderer
	Sitemap   *stm.Sitemap
	WebRoute  webroute.WebRoute
}

// Index determines if the user has authentication and loads the associated page.
func (h *Root) Index(ctx context.Context, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	r.URL.Path = "/site-index.html"
	return web.StaticHandler(ctx, w, r, params, h.StaticDir, "")
}

// SitePage loads the page with the layout for site instead of the app base.
func (h *Root) SitePage(ctx context.Context, w http.ResponseWriter, r *http.Request, params map[string]string) error {

	data := make(map[string]interface{})

	var tmpName string
	switch r.RequestURI {
	case "/":
		r.URL.Path = "/site-index.html"
		return web.StaticHandler(ctx, w, r, params, h.StaticDir, "")
	default:
		return web.Redirect(ctx, w, r, "/", http.StatusFound)
	}

	return h.Renderer.Render(ctx, w, r, TmplLayoutBase, tmpName, web.MIMETextHTMLCharsetUTF8, http.StatusOK, data)
}

// IndexHtml redirects /index.html to the website root page.
func (h *Root) IndexHtml(ctx context.Context, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	return web.Redirect(ctx, w, r, "/", http.StatusMovedPermanently)
}

// RobotHandler returns a robots.txt response.
func (h *Root) RobotTxt(ctx context.Context, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	if webcontext.ContextEnv(ctx) != webcontext.Env_Prod {
		txt := "User-agent: *\nDisallow: /"
		return web.RespondText(ctx, w, txt, http.StatusOK)
	}

	sitemapUrl := h.WebRoute.WebsiteUrl("/sitemap.xml")

	txt := fmt.Sprintf("User-agent: *\nDisallow: /ping\nDisallow: /status\nDisallow: /debug/\nSitemap: %s", sitemapUrl)
	return web.RespondText(ctx, w, txt, http.StatusOK)
}

type SiteMap struct {
	Pages []SiteMapPage `json:"pages"`
}

type SiteMapPage struct {
	Loc        string  `json:"loc" xml:"loc"`
	File       string  `json:"file" xml:"file"`
	Changefreq string  `json:"changefreq" xml:"changefreq"`
	Mobile     bool    `json:"mobile" xml:"mobile"`
	Priority   float64 `json:"priority" xml:"priority"`
	Lastmod    string  `json:"lastmod" xml:"lastmod"`
}

// SitemapXml returns a robots.txt response.
func (h *Root) SitemapXml(ctx context.Context, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	w.Write(h.Sitemap.XMLContent())
	return nil
}
