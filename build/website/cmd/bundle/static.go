package bundle

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/css"
	"github.com/tdewolff/minify/html"
	"github.com/tdewolff/minify/js"
	"github.com/tdewolff/minify/svg"
)

var (
	tagRe    *regexp.Regexp
	cssRelRe *regexp.Regexp
)

func init() {
	tagRe = regexp.MustCompile(`<[img|script|link]([^>]+)>(</script>)?`)
	//attrRe = regexp.MustCompile(`[data-src|src|href]=["']([^"']+)["']`)

	// url(../../assets/icons/font-awesome/css/font-awesome.min.css);
	cssRelRe = regexp.MustCompile(`url[\("'](../[^\)"']+)`)
}

func HTML(log zerolog.Logger, srcDir, staticDir, urlBase string, enableMinify, enableLazyLoad, dryRun bool) error {

	var pathFormatter PathFormatter
	if urlBase != "" {
		u, err := url.Parse(urlBase)
		if err != nil {
			return errors.WithStack(err)
		}
		basePath := u.Path

		pathFormatter = func(s string) string {
			u.Path = filepath.Join(basePath, s)
			return u.String()
		}
	} else {
		pathFormatter = func(s string) string {
			return s
		}
	}

	if !filepath.IsAbs(staticDir) {
		var err error
		staticDir, err = filepath.Abs(staticDir)
		if err != nil {
			return errors.WithStack(err)
		}
	}

	if _, err := os.Stat(staticDir); os.IsNotExist(err) {
		err = os.MkdirAll(staticDir, os.ModePerm)
		if err != nil {
			return errors.WithStack(err)
		}
	}

	var m *minify.M
	if enableMinify {
		m = minify.New()
		m.AddFunc("text/css", css.Minify)
		m.Add("text/html", &html.Minifier{
			KeepDefaultAttrVals: true,
			KeepWhitespace:      true,
		})
		m.AddFunc("image/svg+xml", svg.Minify)
		m.AddFuncRegexp(regexp.MustCompile("^(application|text)/(x-)?(java|ecma)script$"), js.Minify)
	}

	if !filepath.IsAbs(srcDir) {
		var err error
		srcDir, err = filepath.Abs(srcDir)
		if err != nil {
			return errors.WithStack(err)
		}
	}

	err := filepath.Walk(srcDir,
		func(p string, f os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if f.IsDir() || strings.HasPrefix(p, "assets/") {
				// Directory should already exist.
				return nil
			}

			if strings.HasSuffix(f.Name(), ".html") || strings.HasSuffix(f.Name(), ".htm") {
				err = handleHtmlFile(log, p, staticDir, pathFormatter, m, enableLazyLoad, dryRun)
				if err != nil {
					return err
				}
			} else if strings.HasSuffix(f.Name(), ".tmpl") || strings.HasSuffix(f.Name(), ".gohtml") {
				err = handleTemplateFile(log, p, staticDir, pathFormatter, m, enableLazyLoad, dryRun)
				if err != nil {
					return err
				}
			}

			return nil
		})
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func handleHtmlFile(log zerolog.Logger, targetFile, staticDir string, pathFormatter PathFormatter, minify *minify.M, enableLazyLoad, dryRun bool) error {
	dat, err := ioutil.ReadFile(targetFile)
	if err != nil || len(dat) < 5 {
		return errors.WithStack(err)
	}

	// file is actually a GO Template file.
	if strings.HasPrefix(string(dat[0:3]), "{{") {
		return handleTemplateFile(log, targetFile, staticDir, pathFormatter, minify, enableLazyLoad, dryRun)
	}

	log = log.With().Str("file", targetFile).Str("type", "html").Logger()

	start := time.Now()
	size := len(dat)
	defer func() {
		savedPct := ((float64(size) - float64(len(dat))) / float64(size)) * 100
		log.Debug().Dur("dur", time.Now().Sub(start)).Float64("saved", savedPct).Send()
	}()

	dat, err = parseHTML(log, dat, targetFile, staticDir, pathFormatter, minify, enableLazyLoad, dryRun, false)
	if err != nil {
		return err
	}

	if minify != nil {
		dat, err = minify.Bytes("text/html", dat)
		if err != nil {
			return errors.WithStack(err)
		}
	}

	if !dryRun {
		err = ioutil.WriteFile(targetFile, dat, 0644)
		if err != nil {
			return errors.WithStack(err)
		}
	}

	return nil
}

func handleTemplateFile(log zerolog.Logger, targetFile, staticDir string, pathFormatter PathFormatter, minify *minify.M, enableLazyLoad, dryRun bool) error {
	dat, err := ioutil.ReadFile(targetFile)
	if err != nil || len(dat) < 5 {
		return errors.WithStack(err)
	}

	log = log.With().Str("file", targetFile).Str("type", "html").Logger()

	start := time.Now()
	size := len(dat)
	defer func() {
		savedPct := ((float64(size) - float64(len(dat))) / float64(size)) * 100
		log.Debug().Dur("dur", time.Now().Sub(start)).Float64("saved", savedPct).Send()
	}()

	contents := string(dat)

	foundTags := make(map[string]bool)

	tagMatches := tagRe.FindAllStringSubmatch(contents, -1)
	for _, v := range tagMatches {
		foundTags[v[0]] = true
	}

	for foundTag, _ := range foundTags {
		newTag, err := parseHTML(log, []byte("<html><body>"+foundTag+"</body></html>"), targetFile, staticDir, pathFormatter, minify, enableLazyLoad, dryRun, true)
		if err != nil {
			return err
		}

		contents = strings.Replace(contents, foundTag, string(newTag), -1)
	}

	return nil
}

func parseHTML(log zerolog.Logger, dat []byte, targetFile, staticDir string, pathFormatter PathFormatter, minify *minify.M, enableLazyLoad, dryRun, bodyOnly bool) ([]byte, error) {

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(bytes.NewReader(dat))
	if err != nil {
		return dat, errors.WithMessage(err, "Failed to parse HTML")
	}

	doc.Find("img").Each(func(i int, s *goquery.Selection) {
		src, _ := s.Attr("src")
		if src == "" {
			src, _ = s.Attr("data-src")
			if _, hasSrcSet := s.Attr("data-srcset"); src != "" && hasSrcSet {
				if !s.HasClass("lazyload") {
					s.RemoveClass("lazyload")
				}
				return
			}
		}

		if src == "" || strings.Contains(src, "{{") || strings.HasPrefix(src, "//") || strings.HasPrefix(src, "http") {
			return
		}

		u, err := url.Parse(src)
		if err != nil {
			return
		}
		src = u.Path

		fileDir := staticDir
		if !strings.HasPrefix(src, "/") {
			rel, err := filepath.Rel(staticDir, targetFile)
			if err != nil {
				return
			}
			fileDir = filepath.Join(fileDir, filepath.Dir(rel))
		}

		img, err := ImgSrc(log, pathFormatter, fileDir, src, defaultImgSizes, false, enableLazyLoad, dryRun)
		if err != nil {
			log.Err(err).Msgf("Failed to resize image %s", src)
			return
		}

		if enableLazyLoad {
			s.RemoveAttr("src")
			s.RemoveAttr("srcset")
			s.RemoveAttr("sizes")
			if !s.HasClass("lazyload") {
				s.AddClass("lazyload")
			}
		} else {
			s.RemoveAttr("data-src")
			s.RemoveAttr("data-srcset")
			s.RemoveAttr("data-sizes")
			s.RemoveClass("lazyload")
		}

		for k, v := range img.Attrs(enableLazyLoad) {
			s.SetAttr(k, v)
		}
	})

	// Ensure all make ground images are at last no larger that the max responsive image size.
	// style="background-image:url(images/journey-of-the-freckled-indian-75black.jpg)"
	doc.Find("*[style]").Each(func(i int, s *goquery.Selection) {
		style, _ := s.Attr("style")
		if !strings.Contains(style, "background-image:url(") {
			return
		}

		imgUrl := strings.Split(style, "background-image:url(")[1]
		imgUrl = strings.Split(imgUrl, ")")[0]
		imgUrl = strings.Trim(imgUrl, "'")
		imgUrl = strings.Trim(imgUrl, "\"")

		if imgUrl == "" || strings.Contains(imgUrl, "{{") || strings.HasPrefix(imgUrl, "//") || strings.HasPrefix(imgUrl, "http") {
			return
		}

		u, err := url.Parse(imgUrl)
		if err != nil {
			return
		}
		imgUrl = u.Path

		fileDir := staticDir
		if !strings.HasPrefix(imgUrl, "/") {
			rel, err := filepath.Rel(staticDir, targetFile)
			if err != nil {
				return
			}
			fileDir = filepath.Join(fileDir, filepath.Dir(rel))
		}

		maxWidth := []int{defaultImgSizes[len(defaultImgSizes)-1]}

		img, err := ImgSrc(log, pathFormatter, fileDir, imgUrl, maxWidth, false, enableLazyLoad, dryRun)
		if err != nil {
			log.Err(err).Msgf("Failed to resize image %s", imgUrl)
			return
		}

		style = strings.Replace(style, imgUrl, img.src, 1)
		s.SetAttr("style", style)
	})

	doc.Find("*[data-bg-image]").Each(func(i int, s *goquery.Selection) {
		imgUrl, _ := s.Attr("data-bg-image")

		if imgUrl == "" || strings.Contains(imgUrl, "{{") || strings.HasPrefix(imgUrl, "//") || strings.HasPrefix(imgUrl, "http") {
			return
		}

		u, err := url.Parse(imgUrl)
		if err != nil {
			return
		}
		imgUrl = u.Path

		fileDir := staticDir
		if !strings.HasPrefix(imgUrl, "/") {
			rel, err := filepath.Rel(staticDir, targetFile)
			if err != nil {
				return
			}
			fileDir = filepath.Join(fileDir, filepath.Dir(rel))
		}

		img, err := ImgSrc(log, pathFormatter, fileDir, imgUrl, defaultImgSizes, false, enableLazyLoad, dryRun)
		if err != nil {
			log.Err(err).Msgf("Failed to resize image %s", imgUrl)
			return
		}

		if len(img.srcs) > 0 {
			s.SetAttr("data-bg-image-sizes", strings.Join(img.sizes, ","))
			s.SetAttr("data-bg-image-urls", strings.Join(img.srcs, ","))
		}
	})

	doc.Find("link").Each(func(i int, s *goquery.Selection) {
		href, _ := s.Attr("href")
		if href == "" || strings.Contains(href, "{{") || strings.HasPrefix(href, "//") || strings.HasPrefix(href, "http") {
			return
		}

		u, err := url.Parse(href)
		if err != nil {
			return
		}
		href = u.Path

		fileDir := staticDir
		if !strings.HasPrefix(href, "/") {
			rel, err := filepath.Rel(staticDir, targetFile)
			if err != nil {
				return
			}
			fileDir = filepath.Join(fileDir, filepath.Dir(rel))
		}

		if l := strings.ToLower(href); strings.HasSuffix(l, ".css") {
			// Still need to replace urls to fix // in paths breaking on s3 from relative urls
			if strings.HasSuffix(l, "min.css") {
				if !strings.HasPrefix(href, "/") {
					href, err = filepath.Rel(staticDir, filepath.Join(fileDir, href))
					if err != nil {
						return
					}
				}

				href = pathFormatter(href)
				if len(u.Query()) > 0 {
					href += "?" + u.Query().Encode()
				}

				s.SetAttr("href", href)
				return
			}
		} else {
			rel, _ := s.Attr("rel")
			if strings.ToLower(rel) != "stylesheet" {
				return
			}
		}

		newPath, err := minifyFile(log, "text/css", filepath.Join(fileDir, href), staticDir, minify, dryRun)
		if err != nil {
			log.Err(err).Msgf("Failed to minify file %s", href)
			return
		}

		newHref, err := filepath.Rel(staticDir, newPath)
		if err != nil {
			return
		}

		if newHref != "" {
			newHref = pathFormatter(newHref)
			if len(u.Query()) > 0 {
				newHref += "?" + u.Query().Encode()
			}

			s.SetAttr("href", newHref)
		}
	})

	doc.Find("script").Each(func(i int, s *goquery.Selection) {
		src, _ := s.Attr("src")
		if src == "" || strings.Contains(src, "{{") || strings.HasPrefix(src, "//") || strings.HasPrefix(src, "http") {
			return
		}

		u, err := url.Parse(src)
		if err != nil {
			return
		}
		src = u.Path

		fileDir := staticDir
		if !strings.HasPrefix(src, "/") {
			rel, err := filepath.Rel(staticDir, targetFile)
			if err != nil {
				return
			}
			fileDir = filepath.Join(fileDir, filepath.Dir(rel))
		}

		if l := strings.ToLower(src); strings.HasSuffix(l, ".js") {
			if strings.HasSuffix(l, "min.js") {
				if !strings.HasPrefix(src, "/") {
					src, err = filepath.Rel(staticDir, filepath.Join(fileDir, src))
					if err != nil {
						return
					}
				}

				src = pathFormatter(src)
				if len(u.Query()) > 0 {
					src += "?" + u.Query().Encode()
				}

				s.SetAttr("src", src)
				return
			}
		} else {
			srcType, _ := s.Attr("type")
			if strings.ToLower(srcType) != "application/javascript" {
				return
			}
		}

		newPath, err := minifyFile(log, "application/javascript", filepath.Join(fileDir, src), staticDir, minify, dryRun)
		if err != nil {
			log.Err(err).Msgf("Failed to minify file %s", src)
			return
		}

		newSrc, err := filepath.Rel(staticDir, newPath)
		if err != nil {
			return
		}

		if newSrc != "" {
			newSrc = pathFormatter(newSrc)
			if len(u.Query()) > 0 {
				newSrc += "?" + u.Query().Encode()
			}

			s.SetAttr("src", newSrc)
		}
	})

	var (
		deferScripts []string
		blockScripts []string
	)
	doc.Find("script").Each(func(i int, s *goquery.Selection) {
		src, _ := s.Attr("src")
		if src == "" {
			h, _ := goquery.OuterHtml(s)
			blockScripts = append(blockScripts, h)
		} else {
			deferScripts = append(deferScripts, src)
		}
		s.Remove()
	})

	if len(deferScripts) > 0 {
		deferLoad := fmt.Sprintf(`<script type="text/javascript">
				['%s'].forEach(function(src) {
					var script = document.createElement('script');
					script.src = src;
					script.async = false;
					document.head.appendChild(script);
				});
			</script>`, strings.Join(deferScripts, "','"))
		doc.Find("body").AppendHtml(deferLoad)
	}
	if len(blockScripts) > 0 {
		for _, b := range blockScripts {
			doc.Find("body").AppendHtml(b)
		}
	}

	var res string
	if bodyOnly {
		res, err = doc.Find("body").Html()
	} else {
		res, err = doc.Html()
	}
	if err != nil {
		err = errors.WithMessage(err, "Failed to generate HTML.")
		return dat, err
	}

	if dryRun {
		log.Debug().Msg(res)
	}

	//if strings.Contains(targetFile, "site-index") {
	//	fmt.Println(">>>>>>>>>>", res)
	//}

	return []byte(res), nil
}

func minifyFile(log zerolog.Logger, mediatype, targetFile, staticDir string, minify *minify.M, dryRun bool) (string, error) {
	if minify == nil {
		return targetFile, nil
	}

	if l := strings.ToLower(targetFile); strings.HasPrefix(l, "//") || strings.HasPrefix(l, "http") {
		return targetFile, nil
	}

	log = log.With().Str("file", targetFile).Str("type", mediatype).Logger()

	dat, err := ioutil.ReadFile(targetFile)
	if err != nil || len(dat) < 5 {
		return targetFile, errors.WithStack(err)
	}
	size := len(dat)
	start := time.Now()

	/*urlMatches := cssRelRe.FindAllStringSubmatch(string(dat), -1)
	if len(urlMatches) > 0 {
		content := string(dat)
		for _, v := range urlMatches {
			p, err := filepath.Abs(filepath.Join(filepath.Dir(targetFile), v[1]))
			if err != nil {
				return targetFile, err
			}

			newUrl, err := filepath.Rel(staticDir, p)
			if err != nil {
				return targetFile, err
			}

			if strings.Contains(v[1], "../") {
				content = strings.Replace(content, v[1], newUrl, 1 )
			}
		}
		dat = []byte(content)
	}*/

	newFilename := filepath.Base(targetFile)
	if pts := strings.Split(newFilename, "."); len(pts) > 0 {
		pts[len(pts)-1] = "min." + pts[len(pts)-1]
		newFilename = strings.Join(pts, ".")
	}
	newFile := filepath.Join(filepath.Dir(targetFile), newFilename)

	// Skip if the file already exists.
	if _, err := os.Stat(newFile); err == nil {
		return newFile, nil
	}

	dat, err = minify.Bytes(mediatype, dat)
	if err != nil {
		return targetFile, errors.WithStack(err)
	}

	defer func() {
		savedPct := ((float64(size) - float64(len(dat))) / float64(size)) * 100
		log.Debug().Dur("dur", time.Now().Sub(start)).Float64("saved", savedPct).Msgf("Wrote %s", newFilename)
	}()

	if !dryRun {
		err = ioutil.WriteFile(newFile, dat, 0644)
		if err != nil {
			return targetFile, errors.WithStack(err)
		}
	}

	return newFile, nil
}
