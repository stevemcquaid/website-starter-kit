package main

import (
	"os"
	"strings"

	"geeks-accelerator/oss/website-starter-kit/build/website/cmd/bundle"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/urfave/cli"
)

func main() {

	// =========================================================================
	// Logging
	zerolog.SetGlobalLevel(zerolog.DebugLevel)

	log := log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	log = log.With().Stack().Caller().Logger()

	// =========================================================================
	// New CLI application.
	app := cli.NewApp()
	app.Name = "Website Tools"
	app.Version = "1.0.1"
	app.Author = "Lee Brown"
	app.Email = "lee@geeksinthewoods.com"

	app.Commands = []cli.Command{
		{
			Name: "bundle",
			Subcommands: []cli.Command{
				{
					Name: "html",
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:     "path",
							Usage:    "the path of the html files",
							Required: true,
						},
						cli.StringFlag{
							Name:     "static",
							Usage:    "the directory for the output files where they will be served from",
							Required: true,
						},
						cli.StringFlag{
							Name:     "url",
							Usage:    "the url prefix used to prepend the referenced paths with",
							EnvVar:   "STATICFILES_URL_BASE",
							Required: false,
						},
						cli.BoolFlag{
							Name:  "minify",
							Usage: "flag enable minification of files",
						},
						cli.BoolFlag{
							Name:  "lazyload",
							Usage: "flag to enable lazing loading of images",
						},
						cli.BoolFlag{
							Name:  "dryrun",
							Usage: "flag to enable dry run",
						},
					},
					Action: func(c *cli.Context) error {
						if pts := strings.Split(c.Command.FullName(), " "); len(pts) == 2 {
							log = log.With().Str(pts[0], pts[1]).Logger()

						} else {
							log = log.With().Str("cmd", c.Command.FullName()).Logger()
						}

						return bundle.HTML(log, c.String("path"),
							c.String("static"),
							c.String("url"),
							c.Bool("minify"),
							c.Bool("lazyload"),
							c.Bool("dryrun"))
					},
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal().Err(err).Send()
	}
}
