package config

import (
	"fmt"
	"path/filepath"

	"encoding/json"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"gitlab.com/geeks-accelerator/oss/devops/pkg/devdeploy"
)

// Function define the name of a function.
type Function = string

var (
	Function_Example Function = "example_func"
)

// List of function names used by main.go for help.
var FunctionNames = []Function{
	//Function_Example,
}

// NewFunction returns the *devdeploy.ProjectFunction.
func NewFunction(log zerolog.Logger, funcName string, cfg *devdeploy.Config) (*devdeploy.ProjectFunction, error) {

	ctx := &devdeploy.ProjectFunction{
		Name:               fmt.Sprintf("%s-%s-%s", cfg.Env, cfg.ProjectName, funcName),
		CodeDir:            filepath.Join(cfg.ProjectRoot, "cmd", funcName),
		DockerBuildDir:     cfg.ProjectRoot,
		DockerBuildContext: ".",

		// Set the release tag for the image to use include env + function name + commit hash/tag.
		ReleaseTag: devdeploy.GitLabCiReleaseTag(cfg.Env, funcName),
	}

	switch funcName {
	case Function_Example:
		// Function specific configuration here.
	default:
		return nil, errors.Wrapf(devdeploy.ErrInvalidFunction,
			"No function context defined for function '%s'",
			funcName)
	}

	// Append the datadog api key before execution.
	ctx.AwsLambdaFunction.UpdateEnvironment = func(vars map[string]string) error {
		vars["FOO"] = "BAR"
		return nil
	}

	ctx.CodeS3Bucket = cfg.AwsS3BucketPrivate.BucketName
	ctx.CodeS3Key = filepath.Join("src", "aws", "lambda", cfg.Env, ctx.Name, ctx.ReleaseTag+".zip")

	// Set the docker file if no custom one has been defined for the service.
	if ctx.Dockerfile == "" {
		ctx.Dockerfile = filepath.Join(ctx.CodeDir, "Dockerfile")
	}

	return ctx, nil
}

// BuildFunctionForTargetEnv executes the build commands for a target function.
func BuildFunctionForTargetEnv(log zerolog.Logger, awsCredentials devdeploy.AwsCredentials, targetEnv Env, functionName, releaseTag string, dryRun, noCache, noPush bool) error {

	cfg, err := NewConfig(log, targetEnv, awsCredentials)
	if err != nil {
		return err
	}

	targetFunc, err := NewFunction(log, functionName, cfg)
	if err != nil {
		return err
	}

	// Override the release tag if set.
	if releaseTag != "" {
		targetFunc.ReleaseTag = releaseTag
	}

	// Append build args to be used for all functions.
	if targetFunc.DockerBuildArgs == nil {
		targetFunc.DockerBuildArgs = make(map[string]string)
	}

	// funcPath is used to copy the service specific code in the Dockerfile.
	codePath, err := filepath.Rel(cfg.ProjectRoot, targetFunc.CodeDir)
	if err != nil {
		return err
	}
	targetFunc.DockerBuildArgs["code_path"] = codePath

	// commitRef is used by main.go:build constant.
	commitRef := getCommitRef()
	if commitRef == "" {
		commitRef = targetFunc.ReleaseTag
	}
	targetFunc.DockerBuildArgs["commit_ref"] = commitRef

	if dryRun {
		cfgJSON, err := json.MarshalIndent(cfg, "", "    ")
		if err != nil {
			log.Fatal().Err(err).Msgf("BuildFunctionForTargetEnv : Marshalling config to JSON")
		}
		log.Debug().Msgf("BuildFunctionForTargetEnv : config : %v\n", string(cfgJSON))

		detailsJSON, err := json.MarshalIndent(targetFunc, "", "    ")
		if err != nil {
			log.Fatal().Err(err).Msgf("BuildFunctionForTargetEnv : Marshalling details to JSON")
		}
		log.Debug().Msgf("BuildFunctionForTargetEnv : details : %v\n", string(detailsJSON))

		return nil
	}

	return devdeploy.BuildLambdaForTargetEnv(stdLogger(log), cfg, targetFunc, noCache, noPush)
}

// DeployFunctionForTargetEnv executes the deploy commands for a target function.
func DeployFunctionForTargetEnv(log zerolog.Logger, awsCredentials devdeploy.AwsCredentials, targetEnv Env, functionName, releaseTag string, dryRun bool) error {

	cfg, err := NewConfig(log, targetEnv, awsCredentials)
	if err != nil {
		return err
	}

	targetFunc, err := NewFunction(log, functionName, cfg)
	if err != nil {
		return err
	}

	// Override the release tag if set.
	if releaseTag != "" {
		targetFunc.ReleaseTag = releaseTag
	}

	if dryRun {
		cfgJSON, err := json.MarshalIndent(cfg, "", "    ")
		if err != nil {
			log.Fatal().Err(err).Msgf("DeployFunctionForTargetEnv : Marshalling config to JSON")
		}
		log.Debug().Msgf("DeployFunctionForTargetEnv : config : %v\n", string(cfgJSON))

		detailsJSON, err := json.MarshalIndent(targetFunc, "", "    ")
		if err != nil {
			log.Fatal().Err(err).Msgf("DeployFunctionForTargetEnv : Marshalling details to JSON")
		}
		log.Debug().Msgf("DeployFunctionForTargetEnv : details : %v\n", string(detailsJSON))

		return nil
	}

	return devdeploy.DeployLambdaToTargetEnv(stdLogger(log), cfg, targetFunc)
}
